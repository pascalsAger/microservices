from flask import Flask
from flask_celery import make_celery
from flask_sqlalchemy import SQLAlchemy
from random import choice
import string

app = Flask(__name__)
app.config.update(
    CELERY_BROKER_URL='amqp://localhost//',
    CELERY_RESULT_BACKEND='db+postgresql://postgres:test123@localhost/postgress',
    SQLALCHEMY_DATABASE_URI='postgresql://postgres:test123@localhost/postgress',
    SQLALCHEMY_TRACK_MODIFICATIONS = False
)
celery = make_celery(app)
db=SQLAlchemy(app)

class Results(db.Model):
	__tablename__ = "celeryresults"
	id = db.Column(db.Integer, primary_key=True)
	data_ = db.Column(db.String(50))

@app.route('/process/<name>')
def process(name):
	reverse.delay(name)
	return 'I sent a request'

@celery.task(name='flask_routes.reverse')
def reverse(string):
	return (string[::-1])	


@app.route('/insert')
def insertData():
	insert.delay()  #celery call format
	return 'Sent and Async request'

@celery.task(name='flask_routes.insert')
def insert():
	for i in range(500):
		data=''.join(choice(string.ascii_uppercase + string.digits) for _ in range(10))
		results = Results(data_=data)
		db.session.add(results)

	db.session.commit()	
	return 'done'


if __name__ == '__main__':
		app.run(debug=True)	

		'''
		Basic celery and flask route interaction 
		'''