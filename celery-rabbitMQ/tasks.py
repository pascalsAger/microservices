from celery import Celery
import time

'''
psycopg2 and sqlalchemy installed.
Better to use redis and other cache as backend for getting results. But here I use postgres.
'''

app = Celery('tasks', broker='amqp://localhost//', backend='db+postgresql://postgres:test123@localhost/postgress')


@app.task
def reverse(string):
	time.sleep(10)
	return (string[::-1])

'''if __name__=="__main__":
	print(reverse("advith"))'''