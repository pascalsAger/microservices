import connexion
from connexion.resolver import RestyResolver
from flask_sqlalchemy import SQLAlchemy


''' 
SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:test123@localhost/postgress'
SQLALCHEMY_BINDS = {
    'users':        'postgresql://postgres:localhost/postgres'
}'''



if __name__=="__main__":
    app = connexion.FlaskApp(__name__, specification_dir='swagger/')
    app.add_api('indexer.yaml',resolver=RestyResolver)
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:test123@localhost/postgress'
    app.config['SECRET_KEY'] = 'thisissecret'
    db=SQLAlchemy(app)
    app.run(debug=True)